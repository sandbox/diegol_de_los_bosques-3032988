CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The aim of this module is to enhace user experience on
Geolocation Google Maps Api views in order to make easier
the visualization of the selected point.

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/3032988

REQUIREMENTS
------------

This module requires Geolocation Field to work
https://www.drupal.org/project/geolocation

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

1. Download and enable module as usual

2. As we create or configure our view with Geolocation Google Maps Api format,
we need to choose on Advanced Options: Geolocation Google Maps Api
"Show raw locations" in order to receive a list with all points.

3. In the same menu, in the tab Google Map Settings ,
Marker section, we need to give views a route to an image
to mark the points in the Marker Icon Path. In the same route
we need to have two image files, the default one and the -hover image.
For example if we have a /sites/default/files/marker.png route to a file,
we will need a /sites/default/files/marker-hover.png existing on our file system


MAINTAINERS
-----------

Current maintainers:
 * Diego Guillermo (diegol_de_los_bosques) - https://www.drupal.org/u/diegol_de_los_bosques
 * Pedro Peláez (psf_) - https://www.drupal.org/u/psf_

This project has been sponsored by:
SDOS
