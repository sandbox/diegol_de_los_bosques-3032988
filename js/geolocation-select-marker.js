/**
 * @file
 * Javascript for the Geolocation select marker.
 */

/**
 * Callback function when maps is loaded.
 */

(function ($, Drupal) {
    Drupal.behaviors.geolocationLoaded = {
        attach: function (context, settings) {

            waintUntilMapIsAvailable();

            function waintUntilMapIsAvailable() {

                if (Drupal.geolocation.maps === undefined || Drupal.geolocation.maps.length <= 0) {
                    // Wait until maps is defined or have any map in it.
                    setTimeout(waintUntilMapIsAvailable, 200);
                }
                else {
                    // NOW we can actually do something with the map object.
                    for (var mi = 0; mi < Drupal.geolocation.maps.length; ++mi) {
                        var map = Drupal.geolocation.maps[mi];
                        // Add callback when map is loaded.
                        Drupal.geolocation.addMapLoadedCallback(processIcon,map.id);
                        // Adds a callback that will be called once the maps library is loaded. Need for ajax.
                        Drupal.geolocation.addCallback(processIcon);
                    }
                }
            }

            function processIcon(){
                for (var mi = 0; mi < Drupal.geolocation.maps.length; ++mi) {
                    var map = Drupal.geolocation.maps[mi];
                    for (var i = 0; i < map.mapMarkers.length; ++i) {

                        var marker = map.mapMarkers[i];

                        // Get lat and lng from infoWindowContent.
                        // marker.position.lat and marker.position.lng because that position are not rounded.
                        var lat = marker.infoWindowContent.split("latitude\" content=\"");
                        lat = lat[1].split("\">")[0];
                        var lng = marker.infoWindowContent.split("longitude\" content=\"");
                        lng = lng[1].split("\">")[0];

                        // Get the result div by its latitude.
                        var result = $('.geolocation[data-lat="' + lat + '"][data-lng="' + lng + '"]');

                        result.data('gmapMarker', marker);
                        // Add click event to marker.
                        // @see https://stackoverflow.com/a/8770321/6385708 .
                        google.maps.event.addListener(marker, 'click', function () {
                            if (result.attr('data-url') && result.attr('data-url') != '') {
                                window.open(result.attr('data-url'),'_blank');
                            }
                        });

                        // Precalculate the marker hover icon.
                        if (result.data() === undefined) {
                            // If not icon is defined don't process the marker.
                            continue;
                        }
                        // @see https://stackoverflow.com/questions/190852/how-can-i-get-file-extensions-with-javascript/12900504#12900504
                        if (result.data().icon) {
                            var iconExt = result.data().icon.slice((result.data().icon.lastIndexOf(".") - 1 >>> 0) + 2);
                            result.data('hoverIcon', result.data().icon.replace('.' + iconExt, '-hover.' + iconExt));
                        }
                        result.on('mouseover', function () {
                            var myMarker = $(this).data('gmapMarker');
                            myMarker.setIcon($(this).data('hoverIcon'));
                        });

                        result.on('mouseleave', function () {
                            var myMarker = $(this).data('gmapMarker');
                            myMarker.setIcon($(this).data('icon'));
                        });
                    }
                }
            }
        }
    };
})(jQuery, Drupal);
